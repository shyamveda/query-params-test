package com.etihad.microservices.queryparams.model;

import java.io.Serializable;

public class Employee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -475463534175207608L;

	private long id;
	private String departmentName;
	private String qualification;
	private int experience;
	private String designation;
	private String name;

	public Employee(long id, String departmentName, String qualification, int experience, String designation,
			String name) {
		super();
		this.id = id;
		this.departmentName = departmentName;
		this.qualification = qualification;
		this.experience = experience;
		this.designation = designation;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", departmentName=" + departmentName + ", qualification=" + qualification
				+ ", experience=" + experience + ", designation=" + designation + ", name=" + name + "]";
	}

}
