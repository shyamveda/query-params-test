package com.etihad.microservices.queryparams.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.etihad.microservices.queryparams.model.Employee;
import com.etihad.microservices.queryparams.service.QueryParamsService;

@CrossOrigin
@RestController
@RequestMapping("/query-params")
public class TestController {

	private final Logger log = LoggerFactory.getLogger(getClass());
	
	
	@Autowired
	private QueryParamsService queryParamsService;

	
	@RequestMapping(path = "/v1/employee-name", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public Employee getEmployeeByName(@RequestParam("name") String name)  {
		log.info("Inside get employee by name method {}", name);
		Employee employee = queryParamsService.getEmployee(name);
		return employee;
	}
	
	
	@RequestMapping(path = "/v1/employee-name-department", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public Employee getEmployeeByNameAndDesignation(@RequestParam("name") String name, @RequestParam("department") String department)  {
		log.info("Inside getEmployeeByNameAndDesignation query parameters recieved is {} {}", name, department);
		Employee employee = queryParamsService.getEmployee(name, department);
		return employee;
	}
	
	
	@RequestMapping(path = "/v1/employee-name-department-qualification", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public Employee getEmployeeByNameDepartmentAndQualification(@RequestParam("name") String name, @RequestParam("department") String department, @RequestParam("qualification") String qualification)  {
		log.info("Inside getEmployeeByNameDepartmentAndQualification query parameters recieved is {} {}, {}", name, department, qualification);
		Employee employee = queryParamsService.getEmployee(name, department, qualification);
		return employee;
	}
	
	
	@RequestMapping(path = "/v1/list-all-employees", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public List<Employee> getAllEmployees()  {
		log.info("Inside getAllEmployees");
		List<Employee> employees = queryParamsService.getAllEmployees();
		return employees;
	}
	
	
	@RequestMapping(path = "/v1/add-employee", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public List<Employee> addEmployee(Employee employee)  {
		log.info("Inside add employee {}", employee);
		List<Employee> employees = queryParamsService.addEmployee(employee);
		return employees;
	}
	
}
