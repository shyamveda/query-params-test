package com.etihad.microservices.queryparams.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.etihad.microservices.queryparams.model.Employee;

@Service
public class QueryParamsService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	List<Employee> employeeList = constructEmployeeList();

	public Employee getEmployee(String name) {
		log.info("Inside get employee by name method {}", name);
		Employee employee = null;
		for (Employee e : employeeList) {
			if (e.getName().equalsIgnoreCase(name)) {
				employee = e;
				break;
			}
		}
		if (employee == null) {
			employee = new Employee(1, "Operations", "MBA", 5, "Manager", "John");
		}
		log.info("End of get employee by name method {}", name);
		return employee;
	}

	public Employee getEmployee(String name, String departmentName) {
		log.info("Inside getEmployeeByNameAndDesignation - {} {}", name, departmentName);
		Employee employee = null;
		for (Employee e : employeeList) {
			if (e.getName().equalsIgnoreCase(name) && e.getDepartmentName().equalsIgnoreCase(departmentName)) {
				employee = e;
				break;
			}
		}
		if (employee == null) {
			employee = new Employee(2, "Admin", "MBA", 5, "Manager", "Julian");
		}
		log.info("End of getEmployeeByNameAndDesignation object returned is {}", employee);
		return employee;
	}

	public Employee getEmployee(String name, String departmentName, String qualification) {
		log.info("Inside getEmployeeByNameDepartmentAndQualification - {} {}, {}", name, departmentName, qualification);
		Employee employee = null;
		for (Employee e : employeeList) {
			if (e.getName().equalsIgnoreCase(name) && e.getDepartmentName().equalsIgnoreCase(departmentName)
					&& e.getQualification().equalsIgnoreCase(qualification)) {
				employee = e;
				break;
			}
		}
		if (employee == null) {
			employee = new Employee(3, "Accounts", "MA", 5, "Manager", "Victor");
		}
		log.info("End of getEmployeeByNameDepartmentAndQualification obhect returned is - {}", employee);
		return employee;
	}

	private List<Employee> constructEmployeeList() {
		log.info("Start of constructing employee list");
		employeeList = new ArrayList<Employee>();
		employeeList.add(new Employee(1, "Operations", "MBA", 5, "Manager", "John"));
		employeeList.add(new Employee(2, "Admin", "MBA", 5, "Manager", "Julian"));
		employeeList.add(new Employee(3, "Accounts", "MA", 5, "Manager", "Victor"));
		employeeList.add(new Employee(4, "Finance", "MBA", 5, "Manager", "Shan"));
		log.info("End of constructing employee list {}", employeeList);
		return employeeList;
	}

	public List<Employee> getAllEmployees() {
		log.info("Start of getAllEmployees");
		return employeeList;
	}

	public List<Employee> addEmployee(Employee employee) {
		log.info("Start of addEmployee to employee list");
		return employeeList;

	}

}
