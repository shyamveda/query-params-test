FROM ibmjava:8-sfj
RUN apt-get update && apt-get -y install gnupg && apt-get -y install libgcrypt20
ADD target/session-manager.jar session-manager.jar
EXPOSE 8301
ENTRYPOINT ["java", "-jar", "session-manager.jar"]