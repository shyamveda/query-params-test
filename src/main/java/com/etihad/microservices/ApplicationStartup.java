package com.etihad.microservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

@SuppressWarnings("rawtypes")
public class ApplicationStartup implements ApplicationListener{

	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	
	public void onApplicationEvent(ApplicationEvent event) {
		log.info("Application Started successfuly");
		
	}

}
